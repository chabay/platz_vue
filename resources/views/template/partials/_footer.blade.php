{{--
  ./resources/views/template/partials/_footer.blade.php
 --}}


 <div id="wrapper-thank">
   <div class="thank">
     <div class="thank-text">platz</div>
   </div>
 </div>

 <div id="main-container-footer">
   <div class="container-footer">

           <div id="row-1f">
             <div class="text-row-1f"><span class="footer-text">What is Platz</span><br>Platz is a blog showcasing hand-picked free themes, design stuff, free fonts and other resources for web designers.</div>
           </div>

           <div id="row-2f">
             <div class="text-row-2f"><span class="footer-text">How does it work</span><br>Platz offers you all the latest freebies found all over the fourth corners without to pay.</div>
           </div>

           <div id="row-3f">
             <div class="text-row-3f"><span class="footer-text">Get in touch!</span><br>Subscribe our RSS or follow us on Facebook, Google+, Pinterest or Dribbble to keep updated.</div>
           </div>

           <div id="row-4f">
             <div class="text-row-4f"><span class="footer-text">Newsletter</span><br>You will be informed monthly about the latest content avalaible.</div>
             <div id="main_tip_newsletter">
               <form>
                 <input type="text" name="newsletter" id="tip_newsletter_input" autocomplete=off required>
               </form>
             </div>
           </div>

   </div>
 </div>


   <div id="wrapper-copyright">
     <div class="copyright">
       <div class="copy-text object">Copyright © 2016. Template by <a id="copyright-template" href="{{ asset('https://dcrazed.com/') }}">Dcrazed</a></div>

       <div class="wrapper-navbouton">
         <div class="google object">g</div>
         <div class="facebook object">f</div>
         <div class="linkin object">i</div>
         <div class="dribbble object">d</div>
       </div>
     </div>
   </div>
