{{--
  ./resources/views/template/partials/_scripts.blade.php
 --}}

 <script src="{{ asset('js/app.js') }}"></script>
 <script src="{{ asset('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js') }}"></script>
 <script src="{{ asset('js/jquery.scrollTo.min.js') }}"></script>
 <script src="{{ asset('js/jquery.localScroll.min.js') }}"></script>
 <script src="{{ asset('js/jquery-animate-css-rotate-scale.js') }}"></script>
 <script src="{{ asset('js/fastclick.min.js') }}"></script>
 <script src="{{ asset('js/jquery.animate-colors-min.js') }}"></script>
 <script src="{{ asset('js/jquery.animate-shadow-min.js') }}"></script>
 <script src="{{ asset('js/main.js') }}"></script>
