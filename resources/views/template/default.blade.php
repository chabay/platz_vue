{{--
  ./resources/views/template/default.blade.php
 --}}

<!DOCTYPE HTML>
	<html lang="en">

		<head>
			@include('template.partials._head')
		</head>

		<body>

			<div id="app">

				<!-- CACHE -->
				<div class="cache"></div>

				<!-- HEADER -->
				<header-component></header-component>

				<!-- NAVBAR -->
				<nav-component></nav-component>

				<!-- PORTFOLIO -->
				<div id="wrapper-container">
					<router-view></router-view>

				<!-- FOOTER -->
					@include('template.partials._footer')
				</div>

 			</div>

			<!-- SCRIPT -->
			@include('template.partials._scripts')

		</body>
		
	</html>
