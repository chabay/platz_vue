/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Composants

Vue.component('header-component', require('./components/HeaderComponent.vue').default);
Vue.component('nav-component', require('./components/NavComponent.vue').default);

// Filters

  // Tronquer texte
    Vue.filter('truncate', function (text, val, ending) {
        return text.substring(0, val) + (val < text.length ? ending || '...' : '')
    })

  // Formater date
    import Vue from 'vue'
    import VueFilterDateParse from 'vue-filter-date-parse'
    import VueFilterDateFormat from 'vue-filter-date-format';

    Vue.use(VueFilterDateParse)
    Vue.use(VueFilterDateFormat)


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import store from './store/index.js'
import router from './router.js'

const app = new Vue({
    el: '#app',
    router,
    store,
    created() {
      this.$store.dispatch('setRessources')
      this.$store.dispatch('setCategories')
      this.$store.dispatch('setCommentaires')
    }
});
