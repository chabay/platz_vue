// ./resources/js/router.js

import Vue    from 'vue'
import Router from 'vue-router'

// Chargement des composants des différentes routes
import RessourcesIndex from './components/ressources/Index'
import RessourcesShow  from './components/ressources/Show'
import CategoriesShow  from './components/categories/Show'

// Création du routing
Vue.use(Router)
export default new Router({
  routes: [
    {
      // ROUTE PAR DEFAUT
      // PATTERN : /
      // CTRL : RessourcesController
      // ACTION : ressources.index
      path: '/',
      name: 'ressources.index',
      component: RessourcesIndex
    },
    {
      // ROUTE DU DETAIL D'UNE RESSOURCE
      // PATTERN : /ressources/id
      // CTRL : RessourcesController
      // ACTION : ressources.show
      path: '/ressources/:id',
      name: 'ressources.show',
      component: RessourcesShow
    },
    {
      // ROUTE DU DETAIL D'UNE CATEGORIE
      // PATTERN : /categories/id
      // CTRL : CategoriesController
      // ACTION : categories.show
      path: '/categories/:id',
      name: 'categories.show',
      component: CategoriesShow
    },
  ]
})
