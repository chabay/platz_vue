// ./resources/js/store/actions.js



let actions = {

    /**
     * setRessources
     * [Va chercher les ressources dans l'api et les renvoie vers SET_RESSOURCES]
     * @param commit
     */
    setRessources({commit}) {
      //transaction Ajax
      axios.get('api/ressources')
           .then(reponsePHP => (commit('SET_RESSOURCES', reponsePHP.data)))
    },

    /**
     * setCategories
     * [Va chercher les catégories dans l'api et les renvoie vers SET_CATEGORIES]
     * @param commit
     */
    setCategories({commit}) {
      //transaction Ajax
      axios.get('api/categories')
           .then(reponsePHP => (commit('SET_CATEGORIES', reponsePHP.data)))

    },

    /**
     * setCommentaires
     * [Va chercher les commentaires dans l'api et les renvoie vers SET_COMMENTAIRES]
     * @param commit
     */
    setCommentaires({commit}) {
      //transaction Ajax
      axios.get('api/commentaires')
           .then(reponsePHP => (commit('SET_COMMENTAIRES', reponsePHP.data)))
    },

    /**
     * insertCommentaire
     * [Envoie un commentaire vers l'api et le renvoie vers INSERT_COMMENTAIRE]
     * @param  commit
     * @param  {[object]} data   [nouveau commentaire]
     */
    insertCommentaire({commit}, data) {
      axios.post('api/commentaires', data)
           .then(reponsePHP => (commit('INSERT_COMMENTAIRE', reponsePHP.data)))
    }
  }

export default actions
