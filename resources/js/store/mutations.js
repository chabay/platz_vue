// ./resources/js/store/mutations.js


let mutations = {

  /**
   * SET_RESSOURCES
   * [Met les ressources de l'api dans le tableau ressources[]]
   * @param state
   * @param {[json]} data  [toutes les ressources]
   */
  SET_RESSOURCES(state, data) {
    state.ressources = data
  },

  /**
   * SET_CATEGORIES
   * [Met les catégories de l'api dans le tableau catégories[]]
   * @param state
   * @param {[json]} data  [toutes les catégories]
   */
  SET_CATEGORIES(state, data) {
    state.categories = data
  },

  /**
   * SET_COMMENTAIRES
   * [Met les commentaires de l'api dans le tableau commentaires[]]
   * @param state
   * @param {[json]} data  [tous les commentaires]
   */
  SET_COMMENTAIRES(state, data) {
    state.commentaires = data
  },

  /**
   * INSERT_COMMENTAIRE
   * [Ajoute un commentaire dans le tableau commentaires[]]
   * @param state
   * @param {[json]} data  [nouveau commentaire]
   */
  INSERT_COMMENTAIRE(state, data) {
    state.commentaires.push(data)
  },
}

export default mutations
