// ./resources/js/store/getters.js

let getters = {

  /**
   * getRessources
   * [Renvoie toutes les ressources]
   * @param state
   * @return {[array]} [ressources[]]
   */
  getRessources(state) {
    return state.ressources
  },

  /**
   * getRessourceById
   * [Renvoie une ressource par son id]
   * @param state
   * @return {[object]} [ressource{}]
   */
  getRessourceById(state) {
    return function(id) {
      return state.ressources.find(ressource => ressource.id === id)
    }
  },

  /**
   * getRessourcesByCategorieId
   * [Renvoie les ressources filtrées par catégorie]
   * @param state
   * @return {[array]} [ressources par catégorie[]]
   */
  getRessourcesByCategorieId(state) {
    return function(id) {
      return state.ressources.filter(ressources => ressources.categorie.id === id)
    }
  },

  /**
   * getCategories
   * [Renvoie toutes les catégories]
   * @param state
   * @return {[array]} [catégories[]]
   */
  getCategories(state) {
    return state.categories
  },

  /**
   * getCommentaires
   * [Renvoie tous les commentaires]
   * @param state
   * @return {[array]} [commentaires[]]
   */
  getCommentaires(state) {
    return state.commentaires
  },

  /**
   * getCommentairesByRessourceId
   * [Renvoie les commentaires filtrés par ressource]
   * @param state
   * @return {[array]} [commentaires par ressource[]]
   */
  getCommentairesByRessourceId(state) {
    return function(id) {
      return state.commentaires.filter(commentaires => commentaires.ressource_id === id)
    }
  }
}

export default getters
