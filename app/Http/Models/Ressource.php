<?php
   namespace App\Http\Models;
   use Illuminate\Database\Eloquent\Model;

   class Ressource extends Model {
       /**
         * The table associated with the model.
         * @var string
         */
        protected $table = 'ressources';

        /**
         * categorie()
         * Va chercher la catégorie de la ressource
         * @return [type] [description]
         */
        public function categorie() {
          return $this->belongsTo('App\Http\Models\Categorie');
        }

        /**
         * commentaires()
         * Va chercher les commentaires de la ressource
         * @return [type] [description]
         */
        public function commentaires() {
          return $this->hasMany('App\Http\Models\Commentaire');
        }
   }
