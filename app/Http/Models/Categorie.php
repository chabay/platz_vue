<?php
   namespace App\Http\Models;
   use Illuminate\Database\Eloquent\Model;

   class Categorie extends Model {
       /**
         * The table associated with the model.
         * @var string
         */
        protected $table = 'categories';

        /**
         * ressources()
         * Va chercher les ressources de la catégorie
         * @return [type] [description]
         */
        public function ressources() {
          return $this->hasMany('App\Http\Models\Ressource');
        }
   }
