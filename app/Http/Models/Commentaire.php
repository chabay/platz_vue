<?php
   namespace App\Http\Models;
   use Illuminate\Database\Eloquent\Model;

   class Commentaire extends Model {
       /**
         * The table associated with the model.
         * @var string
         */
        protected $table = 'commentaires';
        protected $fillable = ['pseudo', 'texte', 'ressource_id'];

        /**
         * ressources()
         * Va chercher la ressource correspondant au commentaire
         * @return [type] [description]
         */
        public function ressource() {
          return $this->belongsTo('App\Http\Models\Ressource');
        }
   }
