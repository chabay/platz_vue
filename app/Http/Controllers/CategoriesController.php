<?php
       namespace App\Http\Controllers;

       use App\Http\Models\Categorie;
       use Illuminate\Support\Facades\View;

       class CategoriesController extends Controller {
         /**
          * index()
          * [Va chercher la liste des catégories]
          * @return [json] [toutes les catégories]
          */
         public function index(){
              return response()->json(Categorie::all());
         }
       }
