<?php
       namespace App\Http\Controllers;

       use Illuminate\Http\Request;
       use App\Http\Models\Commentaire;
       use Illuminate\Support\Facades\View;

       class CommentairesController extends Controller {
         /**
          * index()
          * [Va chercher la liste des commentaires]
          * @return [json] [tous les commentaires]
          */
         public function index(){
            return response()->json(Commentaire::all());
         }

         /**
          * store()
          * [Insère un nouveau commentaire dans la db]
          * @param  Request $request
          * @return [json] [nouveau commentaire]
          */
         public function store(Request $request) {
           $commentaire = Commentaire::create($request->all());
           return response()->json($commentaire);
         }
       }
