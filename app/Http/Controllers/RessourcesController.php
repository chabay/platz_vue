<?php
       namespace App\Http\Controllers;

       use App\Http\Models\Ressource;
       use Illuminate\Support\Facades\View;

       class RessourcesController extends Controller {
         /**
          * index()
          * [Va chercher la liste des ressources avec leur catégorie]
          * @return [json] [toutes les ressources]
          */
         public function index(){
              return response()->json(Ressource::with('categorie')->latest()->get());
         }
       }
